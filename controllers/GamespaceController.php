<?php
/* zuja 17-12-2022: de basis is gemaakt, waar zou nu de game logica moeten? Normale game logica bestaat uit twee object methods zijnde draw() en update(), daarnaast heb je nog een aantal objecten maar welke zijn dat? */

class GamespaceController
{
    public function __construct()
    {
        $this->gamespace = new GamespaceModel();
    }
    public function create()
    {
        $this->gamespace->create();
        header("Location: /index");
    }

    public function home()
    {
        $gamespace = $this->gamespace->read();
        return view('gamespace', compact('gamespace'));
    }

    public function update()
    {
        $gamespace = $this->gamespace->update();
        return view('update_gamespace', compact('gamespace'));

    }

    public function delete()
    {
        $this->gamespace->delete();
        $this->index();
    }

    public function store()
    {
        $this->gamespace->store();
        $this->index();
    }
}
}
