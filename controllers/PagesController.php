<?php

class PagesController {

    public function index() {
        require 'views/index.view.php';
    }

    public function about() {
        require 'views/about.view.php';
    }

    public function navigation() {
        require 'views/utils/navigation.php';
    }

    public function footer() {
        require 'views/utils/footer.php';
    }
}