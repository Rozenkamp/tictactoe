<?php

//Start PDO CONNECTION, Make connection with a mysql database.
class Connection
{

    public static function make($config)
    {
        try {
            return new PDO($config['connection'] .';dbname=' . $config['name'],
                $config['user'],
                $config['pw'],
                $config['options']);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}
//    End PDO connection