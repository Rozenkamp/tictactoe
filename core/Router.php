<?php
class Router {
    //    protected routes
    protected $routes = [];

    public static function load($file) {
        $router = new static; //$router = new Router;
        require $file;
        return $router;

    }

    //  methode define
    public function define($routes) {
        $this->routes = $routes;


    }
    //Gets all information through URI
    public function get($uri, $controller) {
        $this->routes['GET'][$uri] = $controller;
    }
    //Sends all incoming information through URI
    public function post($uri, $controller) {
        $this->routes['POST'][$uri] = $controller;
    }

    //  method direct, it calls for the given requesttype (get) and breaks away the @ sign in routes.php
    public function direct($uri, $requestType) {
        if(array_key_exists($uri, $this->routes[$requestType])){
            //$this->routes[$requestType][$uri]
            return $this->callAction(...explode('@', $this->routes[$requestType][$uri]));
        }

        throw new Exception('Route not defined');
    }

    protected function callAction($controller, $action) {
        $controller = new $controller;
        if(!method_exists($controller, $action)) {
            throw new Exception('Method does not exist.');
        }
        return $controller->$action();
    }
}