<?php
//Composer: Vendor loads routes into the ClassLoader and Classmap.
require 'vendor/autoload.php';
//Every view will load regardless of what comes after its path.
require 'core/Bootstrap.php';

//Bind the class app config, and put it in a variable box.
App::bind('config', require 'config.php');
//Bind the class app query, and calls connections with PDO
App::bind('query', Connection::make(App::get('config')['database']));

//Class router loads the file routes.php.
// When receiving Uri, request asks for methods and  Router request.
Router::load('routes.php')->direct(Request::uri(), Request::method());
