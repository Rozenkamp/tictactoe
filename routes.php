<?php
//Pass view to Pagescontroller
$router->get('', 'PagesController@index');
$router->get('about', 'PagesController@about');
$router->get('navigation', 'PagesController@navigation');
$router->get('footer', 'PagesController@footer');

//Pass to GamespaceController
$router->get('gamespace', 'GamespaceController@read');
$router->post('create_gamespace', 'GamespaceController@create');
$router->post('update_gamespace', 'GamespaceController@update');
$router->post('store_gamespace', 'GamespaceController@store');
$router->post('delete_gamespace', 'GamespaceController@delete');