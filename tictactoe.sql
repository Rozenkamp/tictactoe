CREATE TABLE gamespace
(
    id       int primary key not null auto_increment,
    player int not null,
    XO     varchar (1),
    cr_at          datetime,
    up_at          datetime,
    del_at         datetime
);

INSERT INTO gamespace (player, XO, cr_at, up_at, del_at)
VALUES ('1', 'X', date(now()),date(now()),NULL);
VALUES ('1', 'O', date(now()),date(now()),NULL);
VALUES ('1', '', date(now()),date(now()),NULL);

DROP table  gamespace;