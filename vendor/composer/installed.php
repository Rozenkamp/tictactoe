<?php return array(
    'root' => array(
        'name' => 'gally/tictactoe',
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'reference' => 'cc4535fde522ddb1d166455e53051f46f40673de',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        'gally/tictactoe' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => 'cc4535fde522ddb1d166455e53051f46f40673de',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
