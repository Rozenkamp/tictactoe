<!doctype html>
<html lang="en">
<head>
    <!-- charset -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- bootstrap style sheet-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
</head>
<body>
<!-- Navigation section -->
<?php require 'views/utils/navigation.php'; ?>
<!-- end Navigation -->
<!-- Title of page-->
<h1>about</h1>
<!-- end title-->
<!-- Start small description-->
<!-- End small description -->
<!-- Begin TicTacToe game -->
<!--end TicTacToe game-->

<!-- begin footer section-->
<?php require 'views/utils/footer.php'; ?>
<!-- end footer section-->
<!-- begin optional scripts section-->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8"
        crossorigin="anonymous"></script>
<!-- end optional scripts -->
</body>
</html>